package br.com.buges.longshort.workercotacaoativos.core.usecase;

import java.time.LocalDate;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import br.com.buges.longshort.base.dto.response.ListaErroEnum;
import br.com.buges.longshort.base.gateway.SalvarGateway;
import br.com.buges.longshort.workercotacaoativos.core.entity.HistoricoCotacao;
import br.com.buges.longshort.workercotacaoativos.core.usecase.dto.AtivoDeadLetterResponse;
import br.com.buges.longshort.workercotacaoativos.core.usecase.dto.AtivoRequest;
import br.com.buges.longshort.workercotacaoativos.core.usecase.dto.HistoricoCotacaoResponse;
import br.com.buges.longshort.workercotacaoativos.core.usecase.gateway.BuscarCodigoCotacoesGateway;
import br.com.buges.longshort.workercotacaoativos.core.usecase.gateway.BuscarCotacoesApiGateway;
import br.com.buges.longshort.workercotacaoativos.core.usecase.gateway.BuscarCotacoesSalvasPorCodigoAtivoGateway;
import br.com.buges.longshort.workercotacaoativos.core.usecase.gateway.ConfiguracaoGateway;
import br.com.buges.longshort.workercotacaoativos.core.usecase.gateway.GerarMensagemDeadLetterGateway;
import br.com.buges.longshort.workercotacaoativos.core.usecase.gateway.GerarMensagemRetryGateway;

/**
 * 
 * @author ggarc
 *
 */
@Component
public class ProcessarCotacaoFechamentoUseCaseImpl implements ProcessarCotacaoFechamentoUseCase {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final BuscarCotacoesApiGateway buscarCotacoesApiGateyway;
	private final BuscarCodigoCotacoesGateway buscarCodigoCotacoesGateway;
	private final SalvarGateway<HistoricoCotacao> salvarGateway;

	public ProcessarCotacaoFechamentoUseCaseImpl(BuscarCotacoesApiGateway buscarCotacoesApiGateyway,
			BuscarCodigoCotacoesGateway buscarCodigoCotacoesGateway, SalvarGateway<HistoricoCotacao> salvarGateway) {
		this.buscarCotacoesApiGateyway = buscarCotacoesApiGateyway;
		this.buscarCodigoCotacoesGateway = buscarCodigoCotacoesGateway;
		this.salvarGateway = salvarGateway;
	}

	@Override
	public Void executar(Void arg0) {
		logger.info("Iniciando coleta das cotações de fechamento...");
		List<String> codigos = buscarCodigoCotacoesGateway.buscarCodigos();
		LocalDate hoje = LocalDate.now();
		codigos.forEach(c -> {
			HistoricoCotacaoResponse response = buscarCotacoesApiGateyway.buscar(c, hoje, hoje, false);
			if (!response.getHistoricos().isEmpty()) {
				HistoricoCotacao ct = response.getHistoricos().get(0);
				logger.info("Salvando cotação do ativo {} na data/hora {}/{}", c, ct.getData(), ct.getHora());
				salvarGateway.salvar(ct);
			}
		});

		return null;
	}

}
