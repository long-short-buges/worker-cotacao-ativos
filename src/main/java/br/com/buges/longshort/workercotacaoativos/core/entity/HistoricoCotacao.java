package br.com.buges.longshort.workercotacaoativos.core.entity;

import java.time.LocalDate;
import java.time.LocalTime;

import br.com.buges.longshort.base.entity.BaseEntity;

/**mantem todo o historico de cotacao de um ativo
 * 
 * @author ggarc
 *
 */
public class HistoricoCotacao extends BaseEntity {

	private String codigoAtivo;
	private Double valor;
	private LocalDate data;
	private LocalTime hora;
	
	public HistoricoCotacao(String codigoAtivo, Double valorCotacao, LocalDate data) {
		super();
		this.codigoAtivo = codigoAtivo;
		this.valor = valorCotacao;
		this.data = data;
	}
	
	public HistoricoCotacao(String id, String codigoAtivo, Double valor, LocalDate data) {
		this.id = id;
		this.codigoAtivo = codigoAtivo;
		this.valor = valor;
		this.data = data;
	}
	
	public HistoricoCotacao() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public String getCodigoAtivo() {
		return codigoAtivo;
	}
	public void setCodigoAtivo(String codigoAtivo) {
		this.codigoAtivo = codigoAtivo;
	}

	public Double getValor() {
		return valor;
	}
	public void setValor(Double valorCotacao) {
		this.valor = valorCotacao;
	}
	public LocalDate getData() {
		return data;
	}
	public void setData(LocalDate data) {
		this.data = data;
	}
	public LocalTime getHora() {
		return hora;
	}
	public void setHora(LocalTime hora) {
		this.hora = hora;
	}

	@Override
	public String toString() {
		return "HistoricoCotacao [codigoAtivo=" + codigoAtivo + ", valorCotacao=" + valor + ", data=" + data
				+ ", hora=" + hora + "]";
	}
}
