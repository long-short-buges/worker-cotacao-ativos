package br.com.buges.longshort.workercotacaoativos.infra.dataprovider.mongo;

import java.time.LocalDate;
import java.time.LocalTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "historicoCotacao")
public class MongoHistoricoCotacaoEntity {

	@Id
	@Field("_id")
	private String id;

	@Field
	private String codigoAtivo;

	@Field
	private Double valor;

	@Field
	private LocalDate data;

	@Field
	private LocalTime hora;
	
	public MongoHistoricoCotacaoEntity() {
	}

	public MongoHistoricoCotacaoEntity(String id, String codigoAtivo, Double valor, LocalDate data) {
		this.id = id;
		this.codigoAtivo = codigoAtivo;
		this.valor = valor;
		this.data = data;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCodigoAtivo() {
		return codigoAtivo;
	}

	public void setCodigoAtivo(String codigoAtivo) {
		this.codigoAtivo = codigoAtivo;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public LocalDate getData() {
		return data;
	}

	public void setData(LocalDate data) {
		this.data = data;
	}

	public LocalTime getHora() {
		return hora;
	}

	public void setHora(LocalTime hora) {
		this.hora = hora;
	}

}
