package br.com.buges.longshort.workercotacaoativos.core.usecase.gateway;

import br.com.buges.longshort.workercotacaoativos.core.usecase.dto.CorrelacaoAtivoResponse;

public interface GerarMensagemCorrelacaoGateway {

	void gerar(CorrelacaoAtivoResponse response);

}
