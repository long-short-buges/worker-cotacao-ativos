package br.com.buges.longshort.workercotacaoativos.infra.dataprovider.kafka;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import br.com.buges.longshort.workercotacaoativos.core.usecase.dto.AtivoRequest;
import br.com.buges.longshort.workercotacaoativos.core.usecase.gateway.GerarMensagemRetryGateway;

@Component
@Profile("prod")
public class ProducerRetry implements GerarMensagemRetryGateway {

	@Value(value = "${app.kafka.retry.cadastroativos.topic-name}")
	private String kafkaTopicName;

	private final KafkaTemplate<String, AtivoRequest> kafkaTemplate;

	public ProducerRetry(KafkaTemplate<String, AtivoRequest> kafkaTemplate) {
		this.kafkaTemplate = kafkaTemplate;
	}

	@Override
	public void gerar(AtivoRequest request) {
		kafkaTemplate.send(kafkaTopicName, request);
	}

}
