package br.com.buges.longshort.workercotacaoativos.core.usecase.gateway;

import java.util.List;

import br.com.buges.longshort.workercotacaoativos.core.entity.HistoricoCotacao;

public interface BuscarCotacoesSalvasPorCodigoAtivoGateway {

	/**
	 * Busca as cotações de determinado ativo já salvo
	 * 
	 * @author ronaldo.lanhellas
	 */

	HistoricoCotacao buscarPrimeiraCotacao(String codigoAtivo);
	
	List<HistoricoCotacao> buscarCotacoes(String codigoAtivo);

}
