package br.com.buges.longshort.workercotacaoativos.core.usecase;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.buges.longshort.workercotacaoativos.core.usecase.gateway.BuscarCotacoesApiGateway;

/**
 * processa evento cadastro de ativo, buscando todas as cotacoes desse
 * @author ggarc
 *
 */
public class ProcessarIntradayCotacaoUseCaseImpl implements ProcessarIntradayCotacaoUseCase {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final BuscarCotacoesApiGateway buscarCotacoesApiGateway;
	
	public ProcessarIntradayCotacaoUseCaseImpl(BuscarCotacoesApiGateway buscarCotacoesApiGateway) {
		this.buscarCotacoesApiGateway = buscarCotacoesApiGateway;
	}

	@Override
	public Void executar(Void arg0) {
		logger.info("Iniciando coleta Intraday das cotações....");
		
		return null;
	}
}
