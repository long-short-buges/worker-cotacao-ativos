package br.com.buges.longshort.workercotacaoativos.infra.dataprovider.http;

import java.time.LocalDate;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import br.com.buges.longshort.base.dto.response.ListaErroEnum;
import br.com.buges.longshort.base.dto.response.ResponseData;
import br.com.buges.longshort.base.dto.response.ResponseDataErro;
import br.com.buges.longshort.workercotacaoativos.core.entity.HistoricoCotacao;
import br.com.buges.longshort.workercotacaoativos.core.usecase.dto.HistoricoCotacaoResponse;
import br.com.buges.longshort.workercotacaoativos.core.usecase.gateway.BuscarCotacoesApiGateway;
import br.com.buges.longshort.workercotacaoativos.infra.dataprovider.http.enums.FuncaoAlphaVantageEnum;
import br.com.buges.longshort.workercotacaoativos.infra.dataprovider.http.enums.OutputsizeAlphaVantageEnum;

@Component
public class CotacoesHttpDataProvider implements BuscarCotacoesApiGateway {

	private final AlphaVantageApi alphaVantageApi;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Value("${app.alphavantage.apikey}")
	private String apikey;

	public CotacoesHttpDataProvider(AlphaVantageApi alphaVantageApi) {
		this.alphaVantageApi = alphaVantageApi;
	}

	@Override
	public HistoricoCotacaoResponse buscar(String codigoAtivo, LocalDate inicio, LocalDate fim, boolean full) {

		HistoricoCotacaoResponse historicoResponse = new HistoricoCotacaoResponse();

		try {

			CotacaoAlphaVantageResponse response = alphaVantageApi
					.buscarCotacoes(
							FuncaoAlphaVantageEnum.TIME_SERIES_DAILY.toString(),
							codigoAtivo.concat(".SA"),
							apikey,
							full ? OutputsizeAlphaVantageEnum.FULL.toString() : OutputsizeAlphaVantageEnum.COMPACT.toString());

			response.getTimeseries().forEach((k, v) -> historicoResponse.getHistoricos()
					.add(new HistoricoCotacao(codigoAtivo, Double.parseDouble(v.getFechamento()), LocalDate.parse(k))));

			// TODO: melhorar tipos das exceptions
		} catch (Exception e) {
			logger.error("Ocorreu um erro ao chamar a AlphaVantageAPI", e);
			ResponseData responseData = new ResponseData();
			responseData.adicionarErro(new ResponseDataErro(
					Objects.nonNull(e.getCause()) ? e.getCause().getMessage() : e.getMessage(), ListaErroEnum.TIMEOUT));
			historicoResponse.setResponse(responseData);
		}

		return historicoResponse;
	}

}
