package br.com.buges.longshort.workercotacaoativos.infra.dataprovider.mongo;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import br.com.buges.longshort.base.gateway.SalvarGateway;
import br.com.buges.longshort.workercotacaoativos.core.entity.HistoricoCotacao;
import br.com.buges.longshort.workercotacaoativos.core.usecase.gateway.BuscarCotacoesSalvasPorCodigoAtivoGateway;

@Component
public class MongoDataProvider implements BuscarCotacoesSalvasPorCodigoAtivoGateway, SalvarGateway<HistoricoCotacao> {

	private final HistoricoCotacaoMongoRepository mongoRepository;

	public MongoDataProvider(HistoricoCotacaoMongoRepository mongoRepository) {
		this.mongoRepository = mongoRepository;
	}

	@Override
	public HistoricoCotacao buscarPrimeiraCotacao(String codigoAtivo) {
		Page<MongoHistoricoCotacaoEntity> pageHist = mongoRepository.findByCodigoAtivo(codigoAtivo, PageRequest.of(0, 1));
		if (pageHist.getNumberOfElements() > 0) {
			MongoHistoricoCotacaoEntity mongoEntity = pageHist.getContent().get(0);
			return new HistoricoCotacao(mongoEntity.getCodigoAtivo(), mongoEntity.getValor(), mongoEntity.getData());
		}else {
			return null;
		}
	}

	@Override
	public HistoricoCotacao salvar(HistoricoCotacao hist) {
		mongoRepository.save(
				new MongoHistoricoCotacaoEntity(hist.getId(), hist.getCodigoAtivo(), hist.getValor(), hist.getData()));
		return hist;
	}

	@Override
	public List<HistoricoCotacao> buscarCotacoes(String codigoAtivo) {
		// TODO Auto-generated method stub
		return null;
	}

}
