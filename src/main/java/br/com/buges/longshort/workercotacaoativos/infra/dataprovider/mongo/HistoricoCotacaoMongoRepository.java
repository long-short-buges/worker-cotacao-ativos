package br.com.buges.longshort.workercotacaoativos.infra.dataprovider.mongo;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HistoricoCotacaoMongoRepository extends MongoRepository<MongoHistoricoCotacaoEntity, String> {

	Page<MongoHistoricoCotacaoEntity> findByCodigoAtivo(String codigoAtivo, Pageable page);
	
}
