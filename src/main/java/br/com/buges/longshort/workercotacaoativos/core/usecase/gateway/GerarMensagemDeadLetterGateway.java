package br.com.buges.longshort.workercotacaoativos.core.usecase.gateway;

import br.com.buges.longshort.workercotacaoativos.core.usecase.dto.AtivoDeadLetterResponse;

public interface GerarMensagemDeadLetterGateway {

	void gerar(AtivoDeadLetterResponse response);

}
