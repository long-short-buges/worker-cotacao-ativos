package br.com.buges.longshort.workercotacaoativos.core.usecase.dto;

public class CorrelacaoAtivoResponse {
	
	private String ativo1;
	private String ativo2;
	private double correlacao;

	public CorrelacaoAtivoResponse(String ativo1, String ativo2, double valorCorrelacao) {
		this.ativo1 = ativo1;
		this.ativo2 = ativo2;
		this.correlacao = valorCorrelacao;
	}
	public double getCorrelacao() {
		return correlacao;
	}
	public void setCorrelacao(double correlacao) {
		this.correlacao = correlacao;
	}
	public String getAtivo1() {
		return ativo1;
	}
	public void setAtivo1(String ativo1) {
		this.ativo1 = ativo1;
	}
	public String getAtivo2() {
		return ativo2;
	}
	public void setAtivo2(String ativo2) {
		this.ativo2 = ativo2;
	}
	
}
