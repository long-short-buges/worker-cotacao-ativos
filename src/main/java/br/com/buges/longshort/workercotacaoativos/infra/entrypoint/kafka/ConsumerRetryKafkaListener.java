package br.com.buges.longshort.workercotacaoativos.infra.entrypoint.kafka;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import org.springframework.kafka.annotation.KafkaHandler;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;

import br.com.buges.longshort.workercotacaoativos.core.usecase.ProcessarEventoRetryCadastroUseCase;
import br.com.buges.longshort.workercotacaoativos.core.usecase.dto.AtivoRequest;

@Component
@KafkaListener(topics = "${app.kafka.retry.cadastroativos.topic-name}")
public class ConsumerRetryKafkaListener {

	private final ProcessarEventoRetryCadastroUseCase processarEventoRetryCadastroUseCase;

	private static final long TEMPO_RETRY_MILISEGUNDOS = 5000;

	public ConsumerRetryKafkaListener(ProcessarEventoRetryCadastroUseCase processarEventoRetryCadastroUseCase) {
		this.processarEventoRetryCadastroUseCase = processarEventoRetryCadastroUseCase;
	}

	@KafkaHandler
	public void processarRetryEventoCadastroAtivo(AtivoRequest request, Acknowledgment acknowledgment,
			@Header(KafkaHeaders.RECEIVED_TIMESTAMP) Long timestamp) throws InterruptedException {

		//TODO: colocar a regra de calculo para o usecase
		long millisegundos = LocalDateTime.now().minus(timestamp, ChronoUnit.MILLIS).getSecond() * 1000l;

		if (millisegundos >= TEMPO_RETRY_MILISEGUNDOS) {
			processarEventoRetryCadastroUseCase.executar(request);
			acknowledgment.acknowledge();
		} else {
			acknowledgment.nack(TEMPO_RETRY_MILISEGUNDOS - millisegundos);
		}
	}

}
