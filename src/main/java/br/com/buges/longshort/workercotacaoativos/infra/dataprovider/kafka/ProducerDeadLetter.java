package br.com.buges.longshort.workercotacaoativos.infra.dataprovider.kafka;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import br.com.buges.longshort.workercotacaoativos.core.usecase.dto.AtivoDeadLetterResponse;
import br.com.buges.longshort.workercotacaoativos.core.usecase.gateway.GerarMensagemDeadLetterGateway;

@Component
@Profile("prod")
public class ProducerDeadLetter implements GerarMensagemDeadLetterGateway {

	@Value(value = "${app.kafka.deadletter.cadastroativos.topic-name}")
	private String kafkaTopicName;

	private final KafkaTemplate<String, AtivoDeadLetterResponse> kafkaTemplate;

	public ProducerDeadLetter(KafkaTemplate<String, AtivoDeadLetterResponse> kafkaTemplate) {
		this.kafkaTemplate = kafkaTemplate;
	}

	@Override
	public void gerar(AtivoDeadLetterResponse response) {
		kafkaTemplate.send(kafkaTopicName, response);
	}

}
