package br.com.buges.longshort.workercotacaoativos.core.usecase.gateway;

import java.util.List;

public interface BuscarCodigoCotacoesGateway {

	List<String> buscarCodigos();

}
