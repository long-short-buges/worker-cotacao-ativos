package br.com.buges.longshort.workercotacaoativos.core.usecase.gateway;

import br.com.buges.longshort.workercotacaoativos.core.usecase.dto.AtivoRequest;

public interface GerarMensagemRetryGateway {

	void gerar(AtivoRequest request);

}
