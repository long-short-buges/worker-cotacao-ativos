package br.com.buges.longshort.workercotacaoativos.infra.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import br.com.buges.longshort.workercotacaoativos.core.usecase.gateway.ConfiguracaoGateway;

@Configuration
public class Configuracao implements ConfiguracaoGateway {

	@Value("${app.qtd-dias-cotacao-cadastroativo}")
	private Integer qtdDiasCotacaoCadastroAtivo;
	
	@Override
	public Integer getQuantidadeDiasParaCotacaoNoCadastroAtivo() {
		return qtdDiasCotacaoCadastroAtivo;
	}

}
