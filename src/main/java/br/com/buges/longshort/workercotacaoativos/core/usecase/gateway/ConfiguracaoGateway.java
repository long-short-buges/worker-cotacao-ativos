package br.com.buges.longshort.workercotacaoativos.core.usecase.gateway;

public interface ConfiguracaoGateway {

	Integer getQuantidadeDiasParaCotacaoNoCadastroAtivo();

}
