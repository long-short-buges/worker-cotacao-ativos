package br.com.buges.longshort.workercotacaoativos.core.entity;

import br.com.buges.longshort.base.entity.BaseEntity;

/**
 * 
 * @author ggarc
 * Reposanvel por manter os ativos do dominio
 * EX. PETR4
 */
public class CorrelacaoAtivo extends BaseEntity {

	private String ativo1;
	private String ativo2;
	
	public CorrelacaoAtivo(String ativo1, String ativo2) {
		this.ativo1 = ativo1;
		this.ativo2 = ativo2;
	}
	public String getAtivo1() {
		return ativo1;
	}
	public void setAtivo1(String ativo1) {
		this.ativo1 = ativo1;
	}
	public String getAtivo2() {
		return ativo2;
	}
	public void setAtivo2(String ativo2) {
		this.ativo2 = ativo2;
	}

}
