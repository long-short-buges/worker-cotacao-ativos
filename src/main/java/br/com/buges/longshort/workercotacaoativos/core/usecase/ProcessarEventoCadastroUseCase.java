package br.com.buges.longshort.workercotacaoativos.core.usecase;

import br.com.buges.longshort.base.usecase.BaseUseCase;
import br.com.buges.longshort.workercotacaoativos.core.usecase.dto.AtivoRequest;

/**
 * processa evento cadastro de ativo, buscando todas as cotacoes desse
 * @author ggarc
 *
 */
public interface ProcessarEventoCadastroUseCase extends BaseUseCase<AtivoRequest, Void> {

	
	
}
