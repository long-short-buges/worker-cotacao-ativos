package br.com.buges.longshort.workercotacaoativos.infra.dataprovider.http.enums;

public enum OutputsizeAlphaVantageEnum {
	
	COMPACT, FULL;

}
