package br.com.buges.longshort.workercotacaoativos.core.usecase;

import br.com.buges.longshort.base.usecase.BaseUseCase;
import br.com.buges.longshort.workercotacaoativos.core.usecase.dto.AtivoRequest;

/**
 * Processa o evento de retry de cadastro de ativo
 * 
 * @author ronaldo.lanhellas
 */

public interface ProcessarEventoRetryCadastroUseCase extends BaseUseCase<AtivoRequest, Void> {

}

