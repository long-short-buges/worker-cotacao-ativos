package br.com.buges.longshort.workercotacaoativos.core.usecase;

import java.time.LocalDate;

import org.springframework.stereotype.Component;

import br.com.buges.longshort.base.dto.response.ListaErroEnum;
import br.com.buges.longshort.base.gateway.SalvarGateway;
import br.com.buges.longshort.base.usecase.BaseUseCase;
import br.com.buges.longshort.workercotacaoativos.core.entity.HistoricoCotacao;
import br.com.buges.longshort.workercotacaoativos.core.usecase.dto.AtivoDeadLetterResponse;
import br.com.buges.longshort.workercotacaoativos.core.usecase.dto.AtivoRequest;
import br.com.buges.longshort.workercotacaoativos.core.usecase.dto.HistoricoCotacaoResponse;
import br.com.buges.longshort.workercotacaoativos.core.usecase.gateway.BuscarCotacoesApiGateway;
import br.com.buges.longshort.workercotacaoativos.core.usecase.gateway.BuscarCotacoesSalvasPorCodigoAtivoGateway;
import br.com.buges.longshort.workercotacaoativos.core.usecase.gateway.ConfiguracaoGateway;
import br.com.buges.longshort.workercotacaoativos.core.usecase.gateway.GerarMensagemDeadLetterGateway;

/**
 * Processa o evento de retry de cadastro de ativo
 * 
 * @author ronaldo.lanhellas
 */

@Component
public class ProcessarEventoRetryCadastroUseCaseImpl extends ProcessarEventoCadastroUseCaseBase
		implements ProcessarEventoRetryCadastroUseCase {

	public ProcessarEventoRetryCadastroUseCaseImpl(GerarMensagemDeadLetterGateway gerarMensagemDeadLetterGateway,
			BuscarCotacoesSalvasPorCodigoAtivoGateway buscarCotacoesSalvasPorCodigoAtivoGateway,
			SalvarGateway<HistoricoCotacao> salvarGateway, BuscarCotacoesApiGateway buscarCotacoesGateway,
			ConfiguracaoGateway configuracaoGateway) {
		super(gerarMensagemDeadLetterGateway, buscarCotacoesSalvasPorCodigoAtivoGateway, salvarGateway,
				buscarCotacoesGateway, configuracaoGateway);
	}

	@Override
	protected HistoricoCotacaoResponse buscarCotacoes(AtivoRequest request, LocalDate inicio, LocalDate fim,
			int tentativas) {
		HistoricoCotacaoResponse responseCotacao = buscarCotacoesGateway.buscar(request.getCodigo(), inicio, fim,
				true);
		if (!responseCotacao.getResponse().getErros().isEmpty()) {

			if (responseCotacao.getResponse().getErros().stream().filter(e -> ListaErroEnum.TIMEOUT.equals(e.getTipo()))
					.count() > 0) {
				if (tentativas < 2) {
					return buscarCotacoes(request, inicio, fim, ++tentativas);
				} else {
					logger.info("Gerando mensagem de DeadLetter do ativo {}", request.getCodigo());
					gerarMensagemDeadLetterGateway
							.gerar(new AtivoDeadLetterResponse(request.getCodigo(), request.getDescricao()));
				}
			} else {
				logger.info("Gerando mensagem de DeadLetter do ativo {}", request.getCodigo());
				gerarMensagemDeadLetterGateway
						.gerar(new AtivoDeadLetterResponse(request.getCodigo(), request.getDescricao()));
			}
		}

		return responseCotacao;
	}

}


